import unittest
import BaseHTTPServer
import mock
from enkom.server import Server, handle_get
from enkom.conf import settings


class TestServer(unittest.TestCase):

    @mock.patch('enkom.server.BaseHTTPServer')
    def test_instance(self, mock_base_http_server):
        mock_enkom = mock.MagicMock()

        s = Server(enkom_instance=mock_enkom, host='127.0.0.1', port=8000)

        self.assertIsInstance(s, Server)
        self.assertEqual(s.enkom, mock_enkom)  # has the enkom instance
        self.assertIsInstance(s, BaseHTTPServer.HTTPServer)  # is based on HTTPServer


class TestResponse(unittest.TestCase):

    def setUp(self):
        self.mock_enkom = mock.MagicMock(name='Enkom')

    def test_handle_index(self):
        self.mock_enkom.get_rendered_page.return_value = 'some html..'

        data, content_type = handle_get(self.mock_enkom, '/')

        self.mock_enkom.get_rendered_page.assert_any_call()
        self.assertEqual(data, self.mock_enkom.get_rendered_page.return_value)
        self.assertEqual(content_type, 'text/html')

    def test_styles(self):
        self.mock_enkom.get_compiles_styles.return_value = 'some css..'

        data, content_type = handle_get(self.mock_enkom, '/{}'.format(settings.STYLE_OUTPUT_FILENAME))

        self.mock_enkom.get_compiles_styles.assert_any_call()
        self.assertEqual(data, self.mock_enkom.get_compiles_styles.return_value)
        self.assertEqual(content_type, 'text/css')

    def test_scripts(self):
        self.mock_enkom.get_concatenated_scripts.return_value = 'some scripts..'

        data, content_type = handle_get(self.mock_enkom, '/{}'.format(settings.SCRIPT_OUTPUT_FILENAME))

        self.mock_enkom.get_concatenated_scripts.assert_any_call()
        self.assertEqual(data, self.mock_enkom.get_concatenated_scripts.return_value)
        self.assertEqual(content_type, 'text/javascript')

    def test_static_rewrite(self):
        pass
