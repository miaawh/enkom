import unittest
import enkom.utils as utils


class TestUtils(unittest.TestCase):

    def test_create_dist_id(self):
        id_1, id_2 = utils.create_dist_id(), utils.create_dist_id()
        self.assertNotEqual(id_1, id_2)

    def test_add_dist_id_to_filename(self):
        filename, dist_id = 'file.txt', '666'
        filename_with_id = utils.add_dist_id_to_filename(dist_id, filename)
        self.assertEqual(filename_with_id, 'file.666.txt')