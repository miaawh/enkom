import unittest
import mock
from enkom.conf import settings
from enkom.compilers.less import LessCompiler
from enkom.compilers.md import MarkdownCompiler


class TestLess(unittest.TestCase):

    @mock.patch('enkom.compilers.less.subprocess')
    def test_compile_file(self, mock_subprocess):

        less_file_path = '/path/to/file.less'
        cwd = '/path/to/context/'

        mock_pipe = mock.MagicMock(name='pipe')
        mock_pipe.communicate.return_value = 'mocked results', ''
        mock_subprocess.Popen.return_value = mock_pipe

        LessCompiler(cwd=cwd).compile_file(less_file_path)

        mock_subprocess.Popen.assert_called_once_with('{} {}'.format(settings.LESSC_BINARY, less_file_path),
                                                      shell=mock.ANY, cwd=cwd,
                                                      stdout=mock.ANY, stdin=mock.ANY,
                                                      stderr=mock.ANY)

        # make the mock return an error instead
        mock_pipe.communicate.return_value = '', 'mocked error'

        with self.assertRaises(LessCompiler.compiler_error):
            LessCompiler(cwd=cwd).compile_file(less_file_path)


class TestMarkdown(unittest.TestCase):

    @mock.patch('enkom.compilers.md.markdown')
    def test_compile_text(self, mock_markdown):
        test_text = 'this is some test text'
        MarkdownCompiler().compile_text(test_text)
        mock_markdown.markdown.assert_called_once_with(test_text)

    @mock.patch('enkom.compilers.md.codecs')
    @mock.patch('enkom.compilers.md.markdown')
    def test_compile_file(self, mock_markdown, mock_codecs):
        test_file_path = '/path/to/a/file.md'
        test_text = 'this is some test text'

        mock_file = mock.MagicMock(name='file')
        mock_file.read.return_value = test_text
        mock_codecs.open.return_value = mock_file

        MarkdownCompiler().compile_file(test_file_path)

        mock_codecs.open.assert_called_once_with(test_file_path, mode=mock.ANY, encoding=mock.ANY)
        mock_markdown.markdown.assert_called_once_with(test_text)