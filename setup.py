from setuptools import setup, find_packages

setup(
    name='enkom',
    version='alpha',
    description='static site generator',
    author='miaawh',
    author_email='miaawh@gmail.com',
    packages=find_packages(),
    provides=['enkom'],
    test_suite='tests',
    entry_points={
        'console_scripts': ['enkom = enkom:cli']
    },
    install_requires=['pyyaml', 'jinja2', 'markdown', 'click'],
    tests_require=['mock'],
)