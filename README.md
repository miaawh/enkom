# Enkom
Yet another static site generator, but with less functionality. It can't even handle multiple pages!

```
#!bash

pip install git+https://bitbucket.org/miaawh/enkom.git
```