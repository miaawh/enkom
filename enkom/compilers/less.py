import subprocess
from enkom.compilers import BaseCompiler
from enkom.conf import settings


class LessCompiler(BaseCompiler):

    def __init__(self, cwd=None):
        self.cwd = cwd

    def _execute_command(self, command):
        pipe = subprocess.Popen(command, shell=True, cwd=self.cwd,
                                stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                stderr=subprocess.PIPE)

        stdout, stderr = pipe.communicate()

        if stderr.strip():
            raise self.compiler_error(stderr)

        return stdout

    def compile_file(self, path):
        """

        Node-binary need to be in "/usr/bin/":
            sudo ln -s /usr/local/bin/node /usr/bin/node
        """
        return self._execute_command('{} {}'.format(settings.LESSC_BINARY, path))