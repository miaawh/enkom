import codecs
import markdown


class MarkdownCompiler(object):

    def compile_file(self, path):
        input_file = codecs.open(path, mode='r', encoding='utf-8')
        text = input_file.read()
        return markdown.markdown(text)

    def compile_text(self, text):
        return markdown.markdown(text)