import os
import shutil
import uuid
import yaml
from compilers.md import MarkdownCompiler

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


def markdown_constructor(loader, node):
    value = loader.construct_scalar(node)
    return MarkdownCompiler().compile_text(value)

yaml.add_constructor(u'!markdown', markdown_constructor)


def parse_content(*args):
    f = open(os.path.join(*args))
    return yaml.load(f, Loader)


def create_new_project(name):
    enkom_root = os.path.dirname(os.path.realpath(__file__))
    template_root = os.path.join(enkom_root, 'conf', 'template')

    new_project_root = os.path.join(os.getcwd(), name)

    if os.path.exists(new_project_root):
        raise OSError('Already exist!')

    shutil.copytree(template_root, new_project_root)


def create_dist_id():
    return str(uuid.uuid4()).replace('-', '')


def add_dist_id_to_filename(dist_id, filename):
    filename_parts = filename.split('.')
    filename_parts.insert(-1, dist_id)
    return '.'.join(filename_parts)