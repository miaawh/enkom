import os
import mimetypes
import BaseHTTPServer
from conf import settings


class FileNotFound(BaseException):
    pass


def static_path_rewrite(path):
    """rewrite to project dir static"""
    if path.startswith(settings.STATIC_URL):
        path = '{}{}'.format(settings.STATIC_DIRNAME, path[7:])
    return path


def get_content_type(path):
    content_type, encoding = mimetypes.guess_type(path)
    content_type = content_type or 'application/octet-stream'
    return content_type


def handle_get(enkom, path):

    if path == '/':
        return enkom.get_rendered_page(), 'text/html'
    elif path == '/{}'.format(settings.STYLE_OUTPUT_FILENAME):
        return enkom.get_compiles_styles(), 'text/css'
    elif path == '/{}'.format(settings.SCRIPT_OUTPUT_FILENAME):
        return enkom.get_concatenated_scripts(), 'text/javascript'
    else:

        path = static_path_rewrite(path)

        try:
            file_path = os.path.join(enkom.project_path, *path.split('/'))
            return open(file_path).read(), get_content_type(path)
        except IOError:
            raise FileNotFound


class ResponseHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def _make_response(self, data, content_type, status_code=200):
        self.send_response(status_code)
        self.send_header('Content-Type', content_type)
        self.end_headers()
        self.wfile.write(data)

    def do_GET(self):
        try:
            data, content_type = handle_get(enkom=self.server.enkom, path=self.path)
            self._make_response(data=data, content_type=content_type)
        except FileNotFound:
            self.send_error(404)
        except Exception as e:
            msg = '<pre>{}</pre>'.format(e)
            self._make_response(msg, 'text/html', status_code=500)

        return


class Server(BaseHTTPServer.HTTPServer):
    def __init__(self, enkom_instance, host, port):
        self.enkom = enkom_instance
        BaseHTTPServer.HTTPServer.__init__(self, (host, port), ResponseHandler)
