import os
import shutil
from utils import parse_content, create_dist_id, add_dist_id_to_filename
from renderer import Renderer
from server import Server
from conf import settings
from compilers.less import LessCompiler


class Enkom(object):

    def __init__(self, project_path):
        """
        project_path = root dir of the project
        """
        self.project_path = project_path

    def get_style_path(self):
        return os.path.join(self.project_path, settings.STYLE_DIRNAME)

    def get_script_path(self):
        return os.path.join(self.project_path, settings.SCRIPT_DIRNAME)

    def get_static_path(self):
        return os.path.join(self.project_path, settings.STATIC_DIRNAME)

    def get_rendered_page(self, dist_id=None):
        r = Renderer(
            project_dir=self.project_path,
            content_yml=self.get_content(),
            dist_id=dist_id
        )
        return r.render().encode(settings.PROJECT_ENCODING)

    def get_content(self):
        return parse_content(self.project_path, settings.CONTENT_FILENAME)

    def get_compiles_styles(self):
        styles = u''

        for filename in os.listdir(self.get_style_path()):
            file_path = os.path.join(self.get_style_path(), filename)
            if not os.path.isdir(file_path):
                if filename.endswith('.less'):
                    styles += LessCompiler(cwd=self.get_style_path()).compile_file(file_path)
                elif filename.endswith('.css'):
                    styles += open(file_path).read()
                else:
                    print('Skipped: {}'.format(file_path))

        return styles

    def get_concatenated_scripts(self):
        scripts = ''

        for filename in os.listdir(self.get_script_path()):
            file_path = os.path.join(self.get_script_path(), filename)
            if filename.endswith('.js'):
                scripts += '\n{}'.format(open(file_path).read())
            else:
                print('Skipped: {}'.format(file_path))

        return scripts

    def dist(self, path):
        try:
            shutil.rmtree(path)
        except OSError:
            pass  # didn't exist

        # create dist dir
        os.makedirs(path)

        # generate dist id
        dist_id = create_dist_id()

        # create files
        open(os.path.join(path, add_dist_id_to_filename(dist_id, settings.STYLE_OUTPUT_FILENAME)), 'w').write(
            self.get_compiles_styles()
        )
        open(os.path.join(path, add_dist_id_to_filename(dist_id, settings.SCRIPT_OUTPUT_FILENAME)), 'w').write(
            self.get_concatenated_scripts()
        )
        open(os.path.join(path, 'index.html'), 'w').write(self.get_rendered_page(dist_id=dist_id))

        # copy static
        shutil.copytree(self.get_static_path(), os.path.join(path, 'static'))

    def server(self, host, port):
        Server(self, host, port).serve_forever()

