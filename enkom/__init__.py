import os
import enkom
import click
import conf
import utils

@click.group()
def cli():
    click.echo('')


@cli.command()
@click.argument('name')
def new(name):
    """Start a new project"""
    click.echo('Creating project {}...'.format(name))
    utils.create_new_project(name=name)


@cli.command()
def test():
    """Start a new project"""
    pass