import os
import enkom
import click
import conf


@click.group()
def cli():
    click.echo('')


@cli.command()
@click.option('-h', '--host', default='127.0.0.1', help='Address that the server listens on')
@click.option('-p', '--port', default=8000, help='Port that the server listens on')
def server(host, port):
    """Start the development server"""
    click.echo('Server listening on {}:{} (Ctrl+C to exit)'.format(host, port))
    enkom.Enkom(project_path=os.getcwd()).server(host=host, port=port)


@cli.command()
@click.option('-d', '--destination', default=conf.settings.DEFAULT_DIST_DIRNAME, help='Destination')
def dist(destination):
    """Package the project for distribution"""
    if destination.startswith('./'):
        destination = destination[2:]
    destination = os.path.join(os.getcwd(), destination)

    click.echo('WARNING! This will empty {}'.format(destination))

    if click.confirm('Do you want to continue?'):
        click.echo('Distributing to {}'.format(destination))
        enkom.Enkom(project_path=os.getcwd()).dist(path=destination)


@cli.command()
def test():
    pass