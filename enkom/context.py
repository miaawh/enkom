import utils
from conf import settings


class Context(object):

    def __init__(self, dist_id=None):
        self.dist_id = dist_id

    def style(self):
        filename = settings.STYLE_OUTPUT_FILENAME
        if self.dist_id:
            filename = utils.add_dist_id_to_filename(self.dist_id, filename)
        return '\n<link rel="stylesheet" href="{}" media="screen" charset="utf-8">'.format(filename)

    def script(self):
        filename = settings.SCRIPT_OUTPUT_FILENAME
        if self.dist_id:
            filename = utils.add_dist_id_to_filename(self.dist_id, filename)
        return '\n<script src="{}" type="text/javascript" charset="utf-8"></script>'.format(filename)