# default settings for Enkom

# path to the lessc binary
LESSC_BINARY = '/usr/local/bin/lessc'

# name of the content file, yaml formatted
CONTENT_FILENAME = 'content.yml'

# default name of the dist dir
DEFAULT_DIST_DIRNAME = '_dist'

# locations where Enkom will look for various files
STYLE_DIRNAME = '_style'
SCRIPT_DIRNAME = '_script'
STATIC_DIRNAME = '_static'

# project encoding (used by the string-encode on the rendered page)
PROJECT_ENCODING = 'utf-8'

# urls used
STATIC_URL = '/static/'

# project page filename, i.e. the input file
PAGE_FILENAME = 'index.html'

# output files
STYLE_OUTPUT_FILENAME = 'style.css'
SCRIPT_OUTPUT_FILENAME = 'script.js'
