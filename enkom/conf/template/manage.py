import os

if __name__ == '__main__':
    os.environ.setdefault("ENKOM_SETTINGS_MODULE", "settings")
    import enkom.commands as commands
    commands.cli()
