import os
import importlib
import default as default_settings_module

ENVIRONMENT_VARIABLE = 'ENKOM_SETTINGS_MODULE'


class Settings(object):
    def __init__(self):

        self._settings = {}

        for setting_key in dir(default_settings_module):
            if setting_key.isupper():
                self._settings[setting_key] = getattr(default_settings_module, setting_key)

        try:
            user_settings_module = importlib.import_module(os.environ.get(ENVIRONMENT_VARIABLE))

            for setting_key in dir(user_settings_module):
                if setting_key.isupper():
                    self._settings[setting_key] = getattr(user_settings_module, setting_key)
        except AttributeError as err:
            pass  # no settings

    def __getattr__(self, item):
        try:
            return self._settings[item]
        except KeyError as err:
            raise err


settings = Settings()