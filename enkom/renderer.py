from jinja2 import Environment, FileSystemLoader
from conf import settings
from context import Context


class Renderer(object):
    def __init__(self,
                 project_dir,
                 content_yml,
                 dist_id=None,
                 template_name=settings.PAGE_FILENAME):

        self._content_yml = content_yml
        self._project_dir = project_dir
        self._template_name = template_name
        self._dist_id = dist_id

        self._env = Environment(loader=FileSystemLoader(searchpath=self._project_dir))

    def _get_template(self):
        return self._env.get_template(self._template_name)

    def render(self):
        c = Context(dist_id=self._dist_id)
        return self._get_template().render(enkom=c, content=self._content_yml)